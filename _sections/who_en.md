# What is it

---
Cryptohagen is a series of informal get-togethers every last Sunday of the month in Copenhagen.

There is a similar event in [Aarhus][cryptoaarhus]

## Who is it for

The meetup is for everyone who uses the Internet in one form or
another.

- You might be studying and needing to research sensitive topics
- Perhaps you live in a problematic relationship and need a way to
  get help without your partner being alerted
- You may be a journalist and need some tools to help
  protect a source
- Or maybe you're a future whistle blower

We can all find ourselves in a situation where we need ways to protect our privacy. We are a friendly group of "nerds" who get together and are ready to start the conversation with you about how you can get better at protecting yourself online. We'll have a talk about your specific needs and come up with some ideas to help you get started.

## What else is Cryptohagen

Cryptohagen is also a friendly community that provides a space to discuss topics like privacy, online safety and security or encryption or any other subject among like-minded people. 

So come on down and say hi, have a chat and a good cup of coffee. All you need to bring is a friendly smile and the wish to get to know some nice people in a relaxed environment.

## Get in touch

We have a [Twitter account][twitter]. Feel free to write us there, a few of us have access to it so we'll be sure to reply as soon as we can.

[cryptoaarhus]:https://cryptoaarhus.dk/index_en.html
[twitter]:https://twitter.com/cryptohagen