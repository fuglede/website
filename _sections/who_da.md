# Hvad er det

---
Cryptohagen er en stribe uformelle og hyggelige sammenkomster som 
afholdes i København hver sidste søndag i måneden.

Der afholdes lignende arrangementer i [Aarhus][cryptoaarhus].

## Hvem er det for

Arrangementet er for alle, der bruger internettet i en eller anden
form.

- Du er måske studerende, og skal søge oplysninger eller lave research om følsomme emner
- Måske lever du i et problematisk parforhold og vil kunne søge hjælp
  og oplysning, uden at din partner bliver gjort opmærksom på det
- En journalist, der har brug for værktøjer til,
  at beskytte en kilde
- Eller du er måske en kommende whistleblower

Der vil være tidspunkter hvor alle har behov for, at kunne bevæge sig
rundt på nettet eller kommunikere med andre, uden at nogen 'lytter
med'. Også dig, der til hverdag ikke mener, du har noget at skjule.

Vi sidder en flok nørder og er klar til at hjælpe dig i gang med, at bruge nogle af de populære og anerkendte sikkerhedsværktøjer; både på din smartphone og på din computer med udgangspunkt i dit behov. Men alt det tager vi en snak om, når vi ses.

## Hvem er det også for
Cryptohagen er også et ’friendly community’ hvor der er plads til at vende tanker og idéer om privatliv, sikkerhed på nettet og kryptering – eller noget helt tredie.

Så kom ned og sig hej, drik en kop lækker kaffe, og få en sludder. Det eneste, du skal have med er godt humør og ønsket om at lære nogle rare mennesker at kende i et afslappet miljø.

## Kom i kontakt

Vi har en [Twitterkonto][twitter]. Du er velkommen til, at skrive til os der. Vi er flere med adgang til kontoen, så vi vil sørge for at svare så hurtigt som muligt.


[cryptoaarhus]:https://cryptoaarhus.dk
[twitter]:https://twitter.com/cryptohagen